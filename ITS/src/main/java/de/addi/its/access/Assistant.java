package de.addi.its.access;

import java.time.LocalDateTime;

import de.addi.its.ticket.Ticket;
import de.addi.its.ticket.TicketTransfer;

public class Assistant
{
	public static final int RET_OK = 1;
	public static final int RET_ERROR = -1;
	
	public static Ticket produceTicket(String desc, String editor, String responsible, LocalDateTime open)
	{
		Ticket t = new Ticket();
		t.setDesc(desc);
		t.setEditor(editor);
		t.setResponsible(responsible);
		t.setOpen(open);
		return t;
	}
	public static TicketTransfer doTransfer(String msg, Ticket t)
	{
		TicketTransfer trans;
		if(t == null)
		{
			trans = Assistant.faultTransfer();
		}
		else
		{
			trans = new TicketTransfer();
			trans.getTickets().add(t);
			trans.setMsg(msg);
			trans.setRetcode(RET_OK);
		}
		return trans;
	}
	
	public static TicketTransfer faultTransfer()
	{
		TicketTransfer trans = new TicketTransfer();
		trans.setMsg("Unbekannter Fehler");
		trans.setRetcode(RET_ERROR);
		return trans;
	}
}
