package de.addi.its.access;


import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.runner.notification.RunNotifier;

import de.addi.its.ticket.*;

/*Data Access Objekt für die HTML-Zugriffsoperationen*/

public class DataAccess
{
	//Map als Datenbankersatz
	private Map<Long, Ticket> db 			= new ConcurrentHashMap<Long, Ticket>();
	private static final DataAccess	dai 	= new DataAccess();
	private static long DEFAULT_NUMBER 		= 1234;
	
	private DataAccess() {}
	
	public static DataAccess getInstance()
	{
		return dai;
	}
	
	//Suche Ticket über die Ticketnummer
	public Ticket getTicketByNum(Long num)
	{
		return ( num == null) ? null : db.get(num);
	}
	
	//Ticket hinzufügen
	public Ticket createTicket(Ticket t)
	{
		synchronized(db)
		{
			if(t.getnum() != null)
			{
				if(getTicketByNum(t.getnum()) != null)
				{
					throw new RuntimeException("Ein Ticket mit dieser Nummer: " + t.getnum() +  "existiert schon!");
				}	
			}
			else
			{
				long maxNum = (db.size() > 0) ? Collections.max(db.keySet()).longValue() : DEFAULT_NUMBER;
				t.setnum(Long.valueOf(++maxNum));
			}
			db.put(t.getnum(), t);
			return t;
		}
	}
	
	//Über ID definiertes Ticket löschen
	public Ticket deleteTicketByNum(long num)
	{
		synchronized (db)
		{
			return db.remove(num);
		}
	}
	
	//Datenbank löschen
	public void deleteDatabase()
	{
		synchronized (db)
		{
			db.clear();
		}
	}
	
	
	static boolean isEmpty(String s)
	{
		return s == null || s.trim().length() <= 0;
	}
}
