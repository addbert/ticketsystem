package de.addi.its.ticket;

import java.time.LocalDateTime;
import javax.xml.bind.annotation.*;

/*Ticket-Objekt*/

@XmlRootElement
public class Ticket
{

	private Long 			num;
	private String			desc, editor, responsible;
	private LocalDateTime 	open;
	
	public Long getnum()							{return num;}
	public void setnum(long num)					{this.num = num;}
	public String getDesc()							{return desc;}
	public void setDesc(String desc) 				{this.desc = desc;	}
	public String getEditor()						{return editor;}
	public void setEditor(String editor) 			{this.editor = editor;	}
	public String getResponsible()					{return responsible;}
	public void setResponsible(String responsible)	{this.responsible = responsible;}
	public LocalDateTime getOpen()					{return open;}
	public void setOpen(LocalDateTime open)			{this.open = open;}
}
