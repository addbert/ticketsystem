package de.addi.its.ticket;

import java.util.*;

import javax.xml.bind.annotation.*;

/*Rückgabe Transfer-Objekt*/

@XmlRootElement
public class TicketTransfer
{
	private Integer retcode;
	private String msg;
	@XmlElement(nillable = true)
	private List<Ticket> tickets = new ArrayList<Ticket>();
	
	public Integer getRetcode()				{return retcode;}
	public void setRetcode(Integer retcode)	{this.retcode = retcode;}
	public String getMsg()					{return msg;}
	public void setMsg(String msg)			{this.msg = msg;}
	public List<Ticket> getTickets()		{return tickets;	}
	
}
