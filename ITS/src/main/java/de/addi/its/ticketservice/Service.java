package de.addi.its.ticketservice;

import java.time.LocalDateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import de.addi.its.ticket.*;
import de.addi.its.access.*;

@Produces(MediaType.TEXT_XML)
@Path(Service.ContentPath)
public class Service
{
	private DataAccess dbaccess = DataAccess.getInstance();
	public static final String ContentPath = "/tickethub";
	
	//Neues Ticket erstellen
	@POST
	public TicketTransfer makeTicket(@FormParam("description") String desc,
							 		 @FormParam("editor") String editor,
							 		 @FormParam("res") String responsible)
	{
		LocalDateTime open = LocalDateTime.now();
		Ticket t = Assistant.produceTicket(desc, editor, responsible, open);
		return Assistant.doTransfer("Neues Ticket erzeugt!", dbaccess.createTicket(t));
	}
	
	//Tickert �ber die Ticketnummer anzeigen lassen
	@GET 
	public TicketTransfer getTicketByNum(@QueryParam("num") String num)
	{
		Ticket t = dbaccess.getTicketByNum(new Long(num.trim()));
		return Assistant.doTransfer("Ticket mit Nummer: " + num, t);
	}
}
