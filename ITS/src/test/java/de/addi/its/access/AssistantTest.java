package de.addi.its.access;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.time.LocalDateTime;

import org.junit.*;

import de.addi.its.ticket.Ticket;
import de.addi.its.ticket.TicketTransfer;


public class AssistantTest
{
	public static final LocalDateTime OPEN = LocalDateTime.now(); 
	
	public AssistantTest() {}
	
    @BeforeClass
    public static void setUpClass() {}
    
    @AfterClass
    public static void tearDownClass() {}
    
    @Before
    public void setUp() {}
    
    @After
    public void tearDown() {}
    
    @Test
    public void testProduceTicket()
    {
    	Ticket expectedTicket = new Ticket();
    	expectedTicket.setDesc("Problem");
    	expectedTicket.setEditor("Max Mustermann");
    	expectedTicket.setResponsible("Max Mustermann");
    	expectedTicket.setOpen(OPEN);
    	Ticket testticket = Assistant.produceTicket("Problem", "Max Mustermann", "Max Mustermann", OPEN);
    	assertEquals(expectedTicket.getnum(), testticket.getnum());
    	assertEquals(expectedTicket.getDesc(), testticket.getDesc());
    	assertEquals(expectedTicket.getEditor(), testticket.getEditor());
    	assertEquals(expectedTicket.getResponsible(), testticket.getResponsible());
    	assertEquals(expectedTicket.getOpen(), testticket.getOpen());
    }
    
    @Test
    public void testFaultTransfer()
    {
    	TicketTransfer expected = new TicketTransfer();
    	expected.setMsg("Unbekannter Fehler");
    	expected.setRetcode(-1);
    	TicketTransfer test = Assistant.faultTransfer();
    	assertEquals(expected.getMsg(), test.getMsg());
    	assertEquals(expected.getRetcode(), test.getRetcode());
    }
    
    @Test
    public void testDoTransfer()
    {
    	Ticket test = new Ticket();
    	test.setnum(1);
    	TicketTransfer result = Assistant.doTransfer("Test123", test);
    	
    	assertEquals("Test123", result.getMsg());
    	assertEquals(test.getnum(), result.getTickets().get(0).getnum());
    	assertEquals(Integer.valueOf(1), result.getRetcode());

    }
}
