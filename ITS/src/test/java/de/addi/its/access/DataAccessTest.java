package de.addi.its.access;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import org.junit.*;

import de.addi.its.ticket.Ticket;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class DataAccessTest 
{
	public DataAccessTest(){}
	
    @BeforeClass
    public static void setUpClass() {}
    
    @AfterClass
    public static void tearDownClass() {}
    
    @Before
    public void setUp() {}
    
    @After
    public void tearDown() {}

	@Test
	public void testGetInstance()
	{
		System.out.println("GetInstance");
		DataAccess expected = DataAccess.getInstance();
		assertSame(expected, DataAccess.getInstance());
	}
	
	@Test
	public void testgetTicketByNum()
	{
		System.out.println("GetTicketByNum");
		Map<Long, Ticket> testdb	= new ConcurrentHashMap<Long, Ticket>();
		Long testkey = (long)1234;
		Ticket expected = new Ticket();
		expected.setnum(testkey);
		testdb.put(expected.getnum(), expected);
		assertEquals(expected, testdb.get(testkey));
	}
	
	@Test
	public void testCreateTicket()
	{
		System.out.println("CreateTicket");
		DataAccess test = DataAccess.getInstance();
		Ticket tocreate = new Ticket();
		Ticket result = test.createTicket(tocreate);
		Ticket expected = new Ticket();
		expected.setnum(1);
		assertEquals(expected.getnum(), result.getnum());
	}
	
	@Test
	public void testDeleteTicketByNum()
	{   
		DataAccess result = DataAccess.getInstance();
		Ticket t1 = new Ticket();
		result.createTicket(t1);
		result.deleteTicketByNum(1);
		assertSame(DataAccess.getInstance(), result);
	}
	
	@Test
	public void testDatabase()
	{
		System.out.println("DeleteDatabase");
		DataAccess result = DataAccess.getInstance();
		for (int i = 0; i < 6; i++)
		{
			result.createTicket(new Ticket());
		}
		result.deleteDatabase();
		
		assertSame(DataAccess.getInstance(), result);
	}
	
	@Test
	public void testIsEmpty()
	{
		System.out.println("IsEmpty");
		String empty = null;
		boolean expected = true;
		assertEquals(expected, DataAccess.isEmpty(empty));
	}
}
