package de.addi.its.ticket;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;

import org.junit.*;

public class TicketTest
{
	public TicketTest() {}
	
    @BeforeClass
    public static void setUpClass() {}
    
    @AfterClass
    public static void tearDownClass() {}
    
    @Before
    public void setUp() {}
    
    @After
    public void tearDown() {}
    
    @Test
    public void testGetNum()
    {
    	System.out.println("GetNumber");
    	Ticket t = new Ticket();
    	Long expected = (long) 1234;
    	t.setnum(1234);
    	Long result = t.getnum();
    	assertEquals(expected, result);
    }
    
    @Test
    public void testSetNum()
    {
    	System.out.println("setNumber");
    	Long num = (long) 1234;
    	Ticket t = new Ticket();
    	t.setnum(num);
    	assertEquals(t.getnum(), num);
    }
  
    @Test
    public void testGetDesc()
    {
    	System.out.println("GetDescription");
    	Ticket t = new Ticket();
    	String expected = "ABCDdcba";
    	t.setDesc(expected);
    	String result = t.getDesc();
    	assertEquals(expected, result);
    }
    
    @Test
    public void testSetDesc()
    {
    	System.out.println("setDescripton");
    	String desc = "ABCDdcba";
    	Ticket t = new Ticket();
    	t.setDesc(desc);
    	assertEquals(t.getDesc(), desc);
    }
    
    @Test
    public void testGetEditor()
    {
    	System.out.println("GetEditor");
    	Ticket t = new Ticket();
    	String expected = "Max Mustermann";
    	t.setEditor(expected);
    	String result = t.getEditor();
    	assertEquals(expected, result);
    }
    
    @Test
    public void testSetEditor()
    {
    	System.out.println("setEditor");
    	String editor = "Max Mustermann";
    	Ticket t = new Ticket();
    	t.setEditor(editor);
    	assertEquals(t.getEditor(), editor);
    }
    
    @Test
    public void testGetResponsible()
    {
    	System.out.println("GetResponsilbe");
    	String expected = "Max Mustermann";
    	Ticket t = new Ticket();
    	t.setResponsible(expected);
    	String result = t.getResponsible();
    	assertEquals(expected, result);
    }
    
    @Test
    public void testSetResponsible()
    {
    	System.out.println("setResponsible");
    	Ticket t = new Ticket();
    	String responsible = "Max Mustermann";
    	t.setResponsible(responsible);
    	assertEquals(t.getResponsible(), responsible);
    }
    
    @Test
    public void testGetOpen()
    {
    	System.out.println("getOpen");
    	LocalDateTime expected = LocalDateTime.now();
    	Ticket t = new Ticket();
    	t.setOpen(expected);
    	LocalDateTime result = t.getOpen();
    	assertEquals(expected, result);
    }
    
    @Test
    public void testSetOpen()
    {
    	System.out.println("setOpen");
    	Ticket t = new Ticket();
    	LocalDateTime open = LocalDateTime.now();
    	t.setOpen(open);
    	assertEquals(t.getOpen(), open);
    }
}