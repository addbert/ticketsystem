package de.addi.its.ticket;

import static org.junit.Assert.assertEquals;

import java.util.*;

import org.junit.*;

public class TicketTransferTest
{
	
	private List<Ticket> expectedlist = new ArrayList<Ticket>();
	private Ticket t1 = new Ticket();
	private Ticket t2 = new Ticket();
			
	public TicketTransferTest() {}
	
    @BeforeClass
    public static void setUpClass() {}
    
    @AfterClass
    public static void tearDownClass() {}
    
    @Before
    public void setUp()
    {
    	t1.setnum(1);
    	t2.setnum(2);
    	expectedlist.add(t1);
    	expectedlist.add(t2);
    }
    
    @After
    public void tearDown() {}
    
    @Test
    public void testGetRetCode()
    {
    	System.out.println("GetReturnCode");
    	Integer expected = -1;
    	TicketTransfer tt = new TicketTransfer();
    	tt.setRetcode(expected);
    	Integer result = tt.getRetcode();
    	assertEquals(expected, result);
    }
    
    @Test
    public void testSetRetCode()
    {
    	System.out.println("setReturnCode");
    	TicketTransfer tt = new TicketTransfer();
    	Integer code = -1;
    	tt.setRetcode(code);
    	assertEquals(tt.getRetcode(), code);
    }
    
    @Test
    public void testGetMsg()
    {
    	System.out.println("GetMessage");
    	String expected = "Message";
    	TicketTransfer tt = new TicketTransfer();
    	tt.setMsg(expected);
    	String result = tt.getMsg();
    	assertEquals(expected, result);
    }
    
    @Test
    public void testSetMsg()
    {
    	System.out.println("setMessage");
    	TicketTransfer tt = new TicketTransfer();
    	String msg = "Message";
    	tt.setMsg(msg);
    	assertEquals(tt.getMsg(), msg);
    }
    
    @Test
    public void testGetTickets()
    {
    	System.out.println("GetTickets");
    	TicketTransfer tt = new TicketTransfer();
    	tt.getTickets().add(t1);
    	tt.getTickets().add(t2);
    	List<Ticket> result = tt.getTickets();
    	assertEquals(expectedlist, result);
    }
}
